﻿Module Module1

    Sub Main(ByVal CmdArgs() As String)
        If CmdArgs.Length < 1 Then
            Return

        End If
        If UBound(CmdArgs) < 0 Then
            Return

        End If

        Dim Filename As String = CmdArgs(0)
        Dim VirtualDMD As Boolean = True
        Dim Color As Boolean = False
        For i As Integer = 0 To UBound(CmdArgs)
            If LCase(CmdArgs(i)) = "novirtualdmd" Or LCase(CmdArgs(i)) = "-novirtualdmd" Then
                VirtualDMD = False

            End If
            If LCase(CmdArgs(i)) = "color" Or LCase(CmdArgs(i)) = "-color" Then
                Color = True

            End If

        Next

        Dim _XDMD As New XDMD.Device(VirtualDMD, True, Color)
        Dim OkayForVid As Boolean = False
        Dim TickStart As Integer = Environment.TickCount
        Dim ImgWorked As Boolean = False
        'If Not OkayForVid Then
        Try
            Dim Surf As New XDMD.Surface(Filename, _XDMD)
            Surf.draw(New Drawing.Rectangle(0, 0, 128, 32), 1)
            _XDMD.RenderWait()

            ImgWorked = True
        Catch ex As Exception
            'MsgBox(ex.Message + vbNewLine + ex.StackTrace)
        End Try
        '_XDMD.VideoIsComplete = True
        Try
            If Not ImgWorked Then
                If _XDMD.PlayVideo(Filename, True, False, New Drawing.Rectangle(0, 0, 128, 32), False) Then
                    Do While Not _XDMD.VideoIsComplete
                        _XDMD.DrawVideoFrame()



                        _XDMD.RenderWait()
                        OkayForVid = True
                        Threading.Thread.Sleep(10)
                        'MsgBox("1")
                    Loop
                End If
            end if
        Catch ex As Exception
            'OkayForVid = False
        End Try
        'If Environment.TickCount - TickStart > 800 And OkayForVid Then
        '    'OkayForVid = True
        '    'Threading.Thread.Sleep(10 * 5000)
        '    'MsgBox("Played")
        'Else
        '    OkayForVid = False

        'End If
        'OkayForVid = False
        'If Not OkayForVid Then

        'End If
        'If UBound(CmdArgs) > 0 Then



        If ImgWorked OrElse OkayForVid Then
                Threading.Thread.Sleep(6 * 1000)
            End If
            'Else
            '    If ImgWorked Then
            '        Threading.Thread.Sleep(6 * 1000)
            '    End If
            'End If
            'End If
            'If Not ImgWorked And OkayForVid Then
            '    Threading.Thread.Sleep(10 * 5000)
            'End If
            Try
            _XDMD.Dispose()

        Catch ex As Exception

        End Try
    End Sub

End Module
