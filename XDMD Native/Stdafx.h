// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once


// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef STRICT
#define STRICT
#endif

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0403		// Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0403	// Change this to the appropriate value to target Windows 2000 or later.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 4.0 or later.
#define _WIN32_IE 0x0400	// Change this to the appropriate value to target IE 5.0 or later.
#endif

#define _ATL_APARTMENT_THREADED
#define _ATL_NO_AUTOMATIC_NAMESPACE

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

// turns off ATL's hiding of some common and often safely ignored warning messages
#define _ATL_ALL_WARNINGS


#include "resource.h"
#include <atlbase.h>
#include <atlcom.h>
#include <Windows.h>

using namespace ATL;

#ifndef __D3DRM_H__
#define __D3DRM_H__
#endif


#include <dshow.h>
#include <d3d9.h>
//#include <vmr9.h>
//#include <evr.h>
#include "qedit.h"
#include "wmcodecdsp.h"
#include "dmodshow.h"
#include "dmoreg.h"
//#include <IL21Dec.h>
//#include <Mpconfig.h>
//#include <Sbe.h>
//#include <initguid.h>
//#include <qnetwork.h>
#include "lusb0_usb.h"
#include "pinDMD3.h"

#pragma warning(disable : 4995)

#pragma warning(push, 3)

#include <sstream>

using namespace std;

#pragma warning(pop)

//#include <comutil.h>
//#include <strsafe.h>

//#include <vfw.h>
#include <windows.h>
//#include <Mmreg.h>

//#include <wmsdk.h>

//#include <vector>
//using namespace std;

#include <stdio.h>
#include <stdlib.h>


//#include <ole2.h>