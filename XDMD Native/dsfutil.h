#define SAFE_RELEASE(ptr) if (ptr != NULL) {ptr.Release(); ptr = NULL;}

#define SAFE_RELEASE_FILTER(ptr) if (ptr != NULL) {ptr->Release(); ptr = NULL;}

#define SAFE_ARRAY_DELETE( x )   \
    if( NULL != x )             \
    {                           \
        delete [] x;            \
        x = NULL;               \
    }

#define OleCheck(x) hr = x; if (FAILED(hr)) return hr;

HRESULT AddFilterByCLSID(
    IGraphBuilder *pGraph,  // Pointer to the Filter Graph Manager.
    const GUID& clsid,      // CLSID of the filter to create.
    LPCWSTR wszName,        // A name for the filter.
    IBaseFilter **ppF);
HRESULT GetUnconnectedPin(
    IBaseFilter *pFilter,   // Pointer to the filter.
    PIN_DIRECTION PinDir,   // Direction of the pin to find.
    IPin **ppPin);
HRESULT GetPin(
    IBaseFilter *pFilter,   // Pointer to the filter.
    PIN_DIRECTION PinDir,   // Direction of the pin to find.
	LPWSTR Id,
    IPin **ppPin);
HRESULT ConnectFilters(
    IGraphBuilder *pGraph, // Filter Graph Manager.
    IPin *pOut,            // Output pin on the upstream filter.
    IBaseFilter *pDest);
HRESULT ConnectFilters(
    IGraphBuilder *pGraph, 
    IBaseFilter *pSrc, 
    IBaseFilter *pDest);
HRESULT SaveGraphFile(IGraphBuilder *pGraph, WCHAR *wszPath);
HRESULT LoadGraphFile(IGraphBuilder *pGraph, const WCHAR* wszName);
