# XDMD

XDMD is a .net framework 2D display library for pinball DMD displays. It currently supports PinDMD version 2 and a virtual DMD window.

Main Website: http://xdmd.info/

## Usage

Basic usage consists of first creating a device. PinDMD2 will automatically be initialised if present. You can also create a virtual DMD window. Drawing to the device consists of using surfaces or fonts and then calling render or renderwait. You can draw surfaces or fonts by using the draw functions of the objects and also set them up to render as transitions. You can also render videos and flash files. Apart from the transitions and video/flash rendering usage is like any 2d drawing library and similar to DirectDraw. A color value of 16 is transparent. All source images, videos and flash files are automatically converted to 16 colors dynamically in real time. Several fonts are included as GIF images so use these for reference if creating your own.

## Licensing
The project is mostly open source licensed under LGPL. XDMD was created in October 2013 by Spesoft.

It requires .net framework 4 and Visual C++ 2008 runtime and visual studio 2010 or newer for development
